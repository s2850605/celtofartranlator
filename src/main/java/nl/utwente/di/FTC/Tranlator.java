package nl.utwente.di.FTC;

public class Tranlator {
    public double getBookPrice(String isbn) {
        double res;
        try {
            res = (Double.parseDouble(isbn) * 1.8) + 32;
            return res;
        } catch (NumberFormatException nfe) {
            return 999.9;
        }
    }
}
