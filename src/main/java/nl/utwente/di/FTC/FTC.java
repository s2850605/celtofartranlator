package nl.utwente.di.FTC;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class FTC extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Tranlator t;
	
    public void init() throws ServletException {
    	t = new Tranlator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "FTC";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>C value: " +
                   request.getParameter("isbn") + "\n" +
                "  <P>F value: " +
                   Double.toString(t.getBookPrice(request.getParameter("isbn"))) +
                "<P>the value is 999.9 if you enter invalid C"+
                "</BODY></HTML>");
  }
  

}
